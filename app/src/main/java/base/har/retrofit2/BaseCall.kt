package base.har.retrofit2

import android.util.MalformedJsonException
import base.har.retrofit.ErrorCustom
import base.har.retrofit.HTTPStatus
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.SocketTimeoutException

suspend fun <T : Any> safeApiCall(call: suspend () -> Response<T>): Any? {

    return safeApiResult(call)
}

private suspend fun <T : Any> safeApiResult(call: suspend () -> Response<T>): Any? {
    val response: Any
    try {
        response = call.invoke()
    } catch (e: Exception) {

        if (e is HttpException) {
           return RetrofitErrorHandling.getHttpErrorMessage(e)
        } else if (e is MalformedJsonException || e is com.google.gson.stream.MalformedJsonException) {
            return  ErrorCustom(HTTPStatus.MY_ERROR,"${e.message.toString()}")
        } else if (e is SocketTimeoutException) {
            return  ErrorCustom(HTTPStatus.MY_ERROR,"Connection timeout. Please try again later.")
        } else if (e is IOException) {
            return  ErrorCustom(HTTPStatus.MY_ERROR,"Connection timeout. Please try again later.")
        } else {
            return  ErrorCustom(HTTPStatus.MY_ERROR,"${e.message.toString()}")
        }
    }

    if(RetrofitErrorHandling.isCallSuccess(response))
        return response.body()
    else
       return RetrofitErrorHandling.getErrorMessage(response)


}





