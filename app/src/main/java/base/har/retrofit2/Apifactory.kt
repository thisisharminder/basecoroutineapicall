package base.har.retrofit2

import base.har.retrofit.Apis
import base.har.retrofit.WebServicesConstants
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Apifactory{
  
    //Creating Auth Interceptor to add api_key query in front of all the requests.
//    private val authInterceptor = Interceptor {chain->
//            val newUrl = chain.request().url()
//                    .newBuilder()
//                    //.addQueryParameter("api_key", AppConstants.tmdbApiKey)
//                    .build()
//
//            val newRequest = chain.request()
//                    .newBuilder()
//                    .url(newUrl)
//                    .build()
//
//            chain.proceed(newRequest)
//        }



   private fun interceptorForLogs(): HttpLoggingInterceptor {
        val interceptor2 = HttpLoggingInterceptor()
        interceptor2.level = HttpLoggingInterceptor.Level.BODY
       return interceptor2
    }


   //OkhttpClient for building http request url
    private val tmdbClient = OkHttpClient().newBuilder()
                                .addInterceptor(interceptorForLogs())
//                                .addInterceptor(authInterceptor)
                                .build()


  
    fun retrofit() : Retrofit = Retrofit.Builder()
                .client(tmdbClient)
                .baseUrl(WebServicesConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()   

  
   val tmdbApi : Apis = retrofit().create(Apis::class.java)

}
