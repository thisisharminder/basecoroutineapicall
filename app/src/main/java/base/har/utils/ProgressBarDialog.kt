package base.har.utils

import android.app.Dialog
import android.content.Context
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import base.har.R
import com.wang.avi.AVLoadingIndicatorView


object ProgressBarDialog {


    var dialog: Dialog? = null
    private var avi: AVLoadingIndicatorView? = null

    fun showProgressBar(activity: Context, title: String) {
        var title = title

        if (dialog != null) {
            if (dialog!!.isShowing) {
                return
            }
        }

        try {
            if ("".equals(title, ignoreCase = true)) {
                title = "Loading..."
            }
            //            title = "Alert";
            dialog = Dialog(activity,
                    R.style.AppTheme)
            //            dialog.getWindow().getAttributes().windowAnimations = R.style.AlertDialog_AppCompat;

            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog!!.window!!.setBackgroundDrawableResource(
                    android.R.color.transparent)
            val layoutParams = dialog!!.window!!
                    .attributes
            layoutParams.dimAmount = .5f
            dialog!!.window!!.addFlags(
                    WindowManager.LayoutParams.FLAG_DIM_BEHIND)
            dialog!!.setCancelable(false)
            dialog!!.setCanceledOnTouchOutside(false)


            dialog!!.setContentView(R.layout.progressbar_dialog)
            val tvProgressText = dialog!!.findViewById(R.id.tvProgressText) as TextView
            avi = dialog!!.findViewById(R.id.avi)
            tvProgressText.text = title
            avi!!.show()
            dialog!!.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    fun dismissProgressDialog() {
        try {
            if (dialog != null) {
                if (dialog!!.isShowing) {
                    avi!!.hide()
                    dialog!!.dismiss()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


}
