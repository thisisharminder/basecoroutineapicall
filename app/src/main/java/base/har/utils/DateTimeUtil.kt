package base.har.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


fun getUTCTime(): Long {
    val cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"))
    return cal.timeInMillis

}


/** Converting from String to Date **/

fun String.getDateWithServerTimeStamp(): String {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
            Locale.getDefault())
    dateFormat.timeZone = TimeZone.getTimeZone("GMT")  // IMP !!!
    try {
        var date: Date = dateFormat.parse(this)
        return date.getStringTimeStampWithDate()

    } catch (e: ParseException) {
        return ""
    }
}


/** Converting from Date to String**/

fun Date.getStringTimeStampWithDate(): String {
    val dateFormat = SimpleDateFormat("dd-MM-yyyy hh:mm a",
            Locale.getDefault())
    dateFormat.timeZone = TimeZone.getTimeZone("GMT")
    return dateFormat.format(this)
}


var format = "yyyy-MM-dd hh:mm a"

fun formatDateToLocal(date: Date): String {

    // null check
    if (date == null) return ""


    var timeZone = ""

    // create SimpleDateFormat object with input format
    val sdf = SimpleDateFormat(format)
    // default system timezone if passed null or empty
    if (timeZone == null || "".equals(timeZone.trim { it <= ' ' }, ignoreCase = true)) {
        timeZone = Calendar.getInstance().timeZone.id
    }
    // set timezone to SimpleDateFormat
    sdf.timeZone = TimeZone.getTimeZone(timeZone)
    // return Date in required format with timezone as String
    return sdf.format(date)
}

fun getLocalDate(utcString: String): String {
    var format = "yyyy-MM-dd HH:mm:ss"  //2018-11-29T11:57:04
    try {
        val formatter = SimpleDateFormat(format)
        formatter.timeZone = TimeZone.getTimeZone("Etc/UTC");
        var utcDate = formatter.parse(utcString)

        return formatDateToLocal(utcDate)
    }
    catch (e:Exception)
    {
        return utcString
    }
}

fun getDate(milliSeconds: Long): String {
    // Create a DateFormatter object for displaying date in specified format.
    val formatter = SimpleDateFormat(format)

    // Create a calendar object that will convert the date and time value in milliseconds to date.
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = milliSeconds
    return formatter.format(calendar.time)
}


fun getDateDateOnly(milliSeconds: Long): String {
    // Create a DateFormatter object for displaying date in specified format.
    var format = "yyyy-MM-dd"
    val formatter = SimpleDateFormat(format)

    // Create a calendar object that will convert the date and time value in milliseconds to date.
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = milliSeconds
    return formatter.format(calendar.time)
}

fun convertDateFormat( date: String): String{

    var spf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
    val newDate = spf.parse(date)
    spf = SimpleDateFormat("dd MMM yyyy hh:mm:ss aaa")
    var dateChanged = spf.format(newDate)

    return dateChanged


}




