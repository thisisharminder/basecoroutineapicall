package base.har.utils;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import base.har.BuildConfig;
import base.har.R;
import com.yalantis.ucrop.UCrop;
import es.dmoral.toasty.Toasty;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.List;


public class Utility {
    private Fragment frag = null;
    private FragmentActivity act = null;
    private long imageName;
    private PassValues passValues;
    private Uri photoURI;


    public Utility(Fragment frag) {
        this.act = frag.getActivity();
        passValues = (PassValues) frag;
        this.frag = frag;
    }

    public void selectImage() {
        final String[] items = {Constants.TAKEPHOTO, Constants.GALLERY, Constants.CANCEL};
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        builder.setTitle(R.string.add_photo);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                switch (items[item]) {
                    case Constants.TAKEPHOTO:
                        camera();
                        break;
                    case Constants.GALLERY:
                        gallery();
                        break;
                    case Constants.CANCEL:
                        dialog.dismiss();
                        break;
                }
            }
        });
        builder.show();
    }

    private void camera() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File dir = new File(Environment.getExternalStorageDirectory(), Constants.PATH);
        if (!dir.exists())
            dir.mkdirs();
        try{
            imageName = System.currentTimeMillis();
            photoURI = FileProvider.getUriForFile(act, BuildConfig.APPLICATION_ID + ".provider",
                    new File(dir.getAbsolutePath(), Constants.IMAGENAME + String.valueOf(imageName) + Constants.EXTENSION));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            frag.startActivityForResult(intent, Constants.PHOTO_REQUEST_CAMERA);
        }catch(Exception e) {
            Log.e("exception",e.toString());
        }

    }

    public void gallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        frag.startActivityForResult(intent, Constants.PHOTO_REQUEST_GALLERY);
    }

    private void crop(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        List<ResolveInfo> list = act.getPackageManager().queryIntentActivities(intent, 0);
        int size = list.size();
        if (size == 0) {
            Toast.makeText(act, "Can't find image cropping app", Toast.LENGTH_SHORT).show();
        } else {
            act.grantUriPermission("com.android.camera", uri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            intent.putExtra("crop", "true");
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("outputX", 800);
            intent.putExtra("outputY", 800);
            intent.putExtra("outputFormat", "JPEG");
            intent.putExtra("noFaceDetection", true);
            intent.putExtra("return-data", true);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            frag.startActivityForResult(intent, Constants.PHOTO_REQUEST_CROP);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        File dir = new File(Environment.getExternalStorageDirectory(), Constants.PATH);
        if (!dir.exists())
            dir.mkdirs();
        switch (requestCode) {

            case Constants.PHOTO_REQUEST_GALLERY:
                imageName = System.currentTimeMillis();

                if (data != null && resultCode == Activity.RESULT_OK) {
                    try {
                        String dataPath=getRealPathFromURI(data.getData());
                        if(dataPath!=null)
                        {
                            File sourceFile = new File(dataPath);
                            File destFile = new File(dir.getAbsolutePath(), Constants.IMAGENAME + String.valueOf(imageName) + Constants.EXTENSION);

                            copyFile(sourceFile, destFile);
                            //   photoURI  = FileProvider.getUriForFile(act, BuildConfig.APPLICATION_ID + ".provider", destFile);
                            photoURI = Uri.parse("file://" + destFile.getAbsolutePath());

                            cropCustom();
                        }
                        else
                        {
                            Toasty.error(act,"Download the image from drive and then upload it.").show();
                        }

                        //crop(photoURI);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case Constants.PHOTO_REQUEST_CAMERA:
                if (resultCode == Activity.RESULT_OK) {
                    File tempFile = new File(dir.getAbsolutePath(), Constants.IMAGENAME + String.valueOf(imageName) + Constants.EXTENSION);
                    photoURI = Uri.parse("file://" + tempFile.getAbsolutePath());
                    cropCustom();
                    // crop(photoURI);

                }
                break;

            case Constants.PHOTO_REQUEST_CROP:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        if (data != null) {
                            File file = new File(dir.getAbsolutePath(), Constants.IMAGENAME + String.valueOf(imageName) + Constants.EXTENSION);
                            Uri uri = Uri.parse("file://" + file.getAbsolutePath());
                            Log.d("URI", "onActivityResult: " + uri);
                            passValues.passImageURI(file);
                        } else {
                            Log.d("", act.getString(R.string.error));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    File tempFile = new File(dir.getAbsolutePath(), Constants.IMAGENAME + System.currentTimeMillis() + Constants.EXTENSION);
                    tempFile.delete();
                    Toast.makeText(act, "" + act.getString(android.R.string.cancel), Toast.LENGTH_SHORT).show();
//                    passValues.passImageURI(null);
                }
                break;

            case UCrop.REQUEST_CROP:
                if (resultCode == Activity.RESULT_OK) {
                    final Uri resultUri = UCrop.getOutput(data);
                    File file = new File(dir.getAbsolutePath(), Constants.IMAGENAME + String.valueOf(imageName) + Constants.EXTENSION);
                    passValues.passImageURI(file);
                }
                break;
            case UCrop.RESULT_ERROR:
                final Throwable cropError = UCrop.getError(data);
                Log.e("Error Image", "onActivityResult: ", cropError);
                break;

            default:
                break;
        }
    }

    public void cropCustom() {
        try {
            UCrop.Options options = new UCrop.Options();
            options.setCropFrameColor(ContextCompat.getColor(act, R.color.colorPrimaryDark));
            options.setShowCropGrid(false);
            options.setToolbarColor(ContextCompat.getColor(act, R.color.colorPrimary));
            options.setHideBottomControls(true);
            UCrop.of(photoURI, photoURI)
                    .withAspectRatio(12, 12)
                    .withMaxResultSize(600, 600)
                    .withOptions(options)
                    .start(act, frag);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }
        FileChannel source = new FileInputStream(sourceFile).getChannel();
        FileChannel destination = new FileOutputStream(destFile).getChannel();
        if (source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        destination.close();
    }

    private String getRealPathFromURI(Uri contentURI) {
        Cursor cursor = act.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            String s = cursor.getString(idx);
            cursor.close();
            return s;
        }
    }



    public Bitmap decodeFile(File f) {
        try {
            // decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE = 512;
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public interface PassValues {
        void passImageURI(File file);
    }



    public static class Constants {

        public static final String IMAGENAME = "Keepook", EXTENSION = ".jpg",
                PATH = "RaysImages",
                TAKEPHOTO = "Take Photo", GALLERY = "Choose from Library", CANCEL = "Cancel";
        public static final int PHOTO_REQUEST_CAMERA = 0, PHOTO_REQUEST_GALLERY = 1, PHOTO_REQUEST_CROP = 2, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

        public static final String GPS_RECIEVER = "gps_receiver";
        public static final Integer PERMISSION_TYPE_LOCATION = 999;
        public static final float GEOFENCE_RADIUS_IN_METERS=5;
    }
}