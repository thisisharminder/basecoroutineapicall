package base.har.utils

import android.content.Context
import android.net.ConnectivityManager


object InternetCheck {


    fun isConnectedToInternet(context: Context): Boolean {
        val connectivity = context
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = connectivity.activeNetworkInfo

        if (activeNetwork != null && activeNetwork.isConnected)
            return true
        else {
            /* Toasty.error(context, context.getString(R.string.internet_connection)).show();*/
            DialogAll(context).showNoInternetDialog()

        }


        return false
    }


    fun isConnectedToInternetWithoutDialog(context: Context): Boolean {
        val connectivity = context
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val activeNetwork = connectivity.activeNetworkInfo

        return activeNetwork != null && activeNetwork.isConnected



    }

}