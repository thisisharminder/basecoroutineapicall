package base.har.base

import android.app.Activity
import android.content.ComponentCallbacks2
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.Window
import base.har.R
import base.har.extensions.view_extensions.setImage
import kotlinx.android.synthetic.main.activity_full_image.*

class ImageShowActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= 21) {
            window.requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
        }
        setContentView(R.layout.activity_full_image)


        val topBanner = intent.getStringExtra("image")

        sdvAds.setImage(this,Uri.parse(topBanner)!!)

    }

    override fun onStop() {
        super.onStop()
        onTrimMemory(ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN)
    }


}


