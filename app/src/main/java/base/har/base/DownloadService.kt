package base.har.base

import android.app.IntentService
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.ResultReceiver
import androidx.core.app.NotificationCompat
import base.har.R
import java.io.BufferedInputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL


class DownloadService : IntentService("DownloadService") {
    private var mNotifyManager: NotificationManager? = null
    private var mBuilder: NotificationCompat.Builder? = null
    override fun onHandleIntent(intent: Intent?) {
        mNotifyManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mBuilder = NotificationCompat.Builder(applicationContext)
        mBuilder!!.setContentTitle("File Download")
            .setContentText("Download in progress")
            .setSmallIcon(R.mipmap.ic_launcher)

        val receiver = intent?.getParcelableExtra("receiver") as ResultReceiver
        val bundle = Bundle()


        val urlToDownload = intent.getStringExtra("url")

        val root = android.os.Environment.getExternalStorageDirectory()
        val dir = File(root.absolutePath + "/keepook/files")
        if (!dir.exists()) {
            dir.mkdirs()
        }
        val nameFile: List<String> = urlToDownload.split("/")
        val file = File(dir, nameFile.last())

        try {

            //create url and connect
            val url = URL(urlToDownload)
            val connection = url.openConnection()
            connection.connect()

            // this will be useful so that you can show a typical 0-100% progress bar
            val fileLength = connection.contentLength

            // download the file
            val input = BufferedInputStream(connection.getInputStream())

            //            String path = "/sdcard/BarcodeScanner-debug.apk" ;
            //            OutputStream output = new FileOutputStream(path);

            val data = ByteArray(fileLength)
            var total: Long = 0
            var count: Int
            while (input.read(data) != -1) {
                count = input.read(data)
                total += count.toLong()

                // publishing the progress....
                val progress=(total * 100 / fileLength).toInt()
                mBuilder!!.setProgress(100, progress, false)
                // Displays the progress bar for the first time.
                mNotifyManager!!.notify(UPDATE_PROGRESS, mBuilder!!.build())
                bundle.putInt("progress", progress)
                receiver.send(DOWNLOAD_PROGRESS, bundle)
            }

            val fos = FileOutputStream(file)
            fos.write(data)
            fos.flush()
            fos.close()
            input.close()

        } catch (e: IOException) {
            receiver.send(DOWNLOAD_ERROR, Bundle.EMPTY);
            e.printStackTrace();
        }


        // publishing the progress....
        mBuilder?.setProgress(100, 100, false)
        bundle.putString("filePath", file.absolutePath)
        bundle.putInt("progress", 100)
        receiver.send(DOWNLOAD_SUCCESS, bundle)
        // Displays the progress bar for the first time.
        mNotifyManager!!.notify(UPDATE_PROGRESS, mBuilder!!.build())


        //        Bundle resultData = new Bundle();
        //        resultData.putInt("progress" ,100);
        //
        //        receiver.send(UPDATE_PROGRESS, resultData);
    }

    companion object {
        val DOWNLOAD_PROGRESS=321
        val DOWNLOAD_SUCCESS=322
        val DOWNLOAD_ERROR: Int=122
        val UPDATE_PROGRESS = 8344
    }
}