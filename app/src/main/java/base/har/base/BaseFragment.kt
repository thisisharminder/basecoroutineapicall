package base.har.base


import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import base.har.R
import base.har.ui.HomeActivity
import base.har.utils.Utility
import kotlinx.android.synthetic.main.main_toolbar_login.view.*


/**
 * Created by harminder on 27/3/18.
 */
abstract class BaseFragment : Fragment(), BaseView {

    var baseView: View? = null

    @LayoutRes
    protected abstract fun getContentLayoutResId(): Int


    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View? {

        if (baseView == null) {
            baseView = inflater.inflate(getContentLayoutResId(), container, false)
        }

        return baseView
    }


    open fun backPress() {}


    private lateinit var baseActivity: BaseActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity) {
            this.baseActivity = context
            //      this.baseActivity.onFragmentAttached();
        }
    }


    protected fun replaceFragment(@NonNull fragment: Fragment) {
        baseActivity.replaceFragment(fragment)

    }

    override fun onDestroyView() {
//        if (unbinder != null) {
//            unbinder.unbind()
//        }
        super.onDestroyView()
        view?.parent?.apply {
            val parent = this as ViewGroup
            parent.removeAllViews()
        }

    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun showLoading(message: String) {
        baseActivity.showLoading(message)
    }


    override fun hideLoading() {
        baseActivity.hideLoading()
    }

    override fun onUnknownError(error: String) {
        baseActivity.onUnknownError(error)
    }

    override fun onNetworkError() {
        baseActivity.onNetworkError()
    }

    override fun onTimeout() {
        baseActivity.onTimeout()
    }

    override fun isNetworkAvailable(): Boolean {
        baseActivity.isNetworkAvailable()
        return false
    }

    override fun onConnectionError() {

        baseActivity?.onConnectionError()

    }


    fun bottomNavigationHide() {
          (baseActivity as HomeActivity).navigationDrawerShow(false)
    }

    fun bottomNavigationShow(isShow: Boolean) {
         (baseActivity as HomeActivity).navigationDrawerShow(isShow)
    }

    fun hideKeyBoard(input: View?) {

        input?.let {
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(input.windowToken, 0)
        }
    }


    fun setToolbar(name: String, drawable: Int?, toolbar2: View) {
        toolbar2.toolbar?.apply {
            navigationIcon?.setTint(ContextCompat.getColor(activity!!, R.color.white))
            if (drawable != null)
                setNavigationIcon(drawable)
            setNavigationOnClickListener {
                activity?.onBackPressed()
            }
        }
        toolbar2.tvTitle.text = name
    }


    fun setToolbarHideback(name: String, toolbar: Toolbar) {
        toolbar.navigationIcon = null
        setToolbar(name, null, toolbar)
    }



    fun setToolbarWithBack(name: String, drawable: Int?, toolbar: Toolbar) {
        // toolbar.title = name
        if (drawable != null)
            toolbar.setNavigationIcon(drawable)

        toolbar.navigationIcon?.setTint(ContextCompat.getColor(activity!!, R.color.colorPrimary))
        toolbar.setNavigationOnClickListener {
            backPress()
            hideKeyBoard(toolbar)

        }

        if (name.isNotEmpty())
            toolbar.tvTitle.setCompoundDrawables(null, null, null, null)
        toolbar.tvTitle.text = name


    }


    fun setToolbar(name: String, drawable: Int?, toolbar: Toolbar) {
        // toolbar.title = name
        if (drawable != null)
            toolbar.setNavigationIcon(drawable)

        toolbar.navigationIcon?.setTint(ContextCompat.getColor(activity!!, R.color.colorPrimary))
        toolbar.setNavigationOnClickListener {
            backPress()
            hideKeyBoard(toolbar)
            activity?.onBackPressed()
        }

        if (name.isNotEmpty())
            toolbar.tvTitle.setCompoundDrawables(null, null, null, null)
        toolbar.tvTitle.text = name


    }

    fun setToolbarHome(name: String, toolbar: Toolbar) {
        // toolbar.title = name
        //toolbar.setNavigationIcon(R.drawable.ic)

        toolbar.navigationIcon?.setTint(ContextCompat.getColor(activity!!, R.color.white))
        toolbar.setNavigationOnClickListener {
            //  (activity as HomeActivity).openCloseDrawer()
        }
        toolbar.tvTitle.text = name


    }

    fun checkPermission(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(context!!, permission) == PackageManager.PERMISSION_GRANTED
    }

    fun checkPermission(context: Context): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(
                    context,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    val alertBuilder = AlertDialog.Builder(context)
                    alertBuilder.setCancelable(true)
                    alertBuilder.setTitle(R.string.permission_neccassary)
                    alertBuilder.setMessage(R.string.storage_permission)
                    alertBuilder.setPositiveButton(android.R.string.yes) { dialog, which ->
                        requestPermissions(
                            arrayOf(
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ), Utility.Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
                        )
                    }
                    val alert = alertBuilder.create()
                    alert.show()
                } else {
                    requestPermissions(
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        Utility.Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE
                    )
                }
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }

    fun setNoDataLayoutvisibility(isEmpty: Boolean, clNoData: ConstraintLayout?) {
        if (isEmpty)
            clNoData?.visibility = View.VISIBLE
        else
            clNoData?.visibility = View.GONE
    }

    private var mLastClickTime = 0L
    fun isValidClick(): Boolean {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
            return false
        } else {
            mLastClickTime = SystemClock.elapsedRealtime()
            return true
        }
    }




}