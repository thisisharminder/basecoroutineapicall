package base.har.retrofit

object HTTPStatus {
    val STATUS_SUCCESS201 = 201
    val STATUS_INVALID_TOKEN = 401
    val BAD_REQUEST = 400
    val STATUS_SUCCESS200 = 200
    val STATUS_SUCCESS202 = 202
    val CUSTOMER_BLOCKED = 403
    val CREATE_ACCOUNT = 204
    val NOT_FOUND = 404
    val USER_ALREADY_CHECKED_IN = 409
    val INTERNAL_SERVER_ERROR = 500


    val ACCOUNT_NOT_EXIST = 402

        // custom

    val MY_ERROR: Int=490
}
