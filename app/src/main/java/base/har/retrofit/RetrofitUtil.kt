package base.har.retrofit

import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit


object RetrofitUtil {

    fun stringToRequestBody(string: String): RequestBody {

        return RequestBody.create(
            MediaType.parse("multipart/form-data"), string)
    }

    fun imageToRequestBody(file: File, fieldName: String): MultipartBody.Part {
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)

        // MultipartBody.Part is used to send also the actual file name

        return MultipartBody.Part.createFormData(fieldName, file.name, requestFile)
    }


    fun ImageToRequestBody(string: File): RequestBody {

        return RequestBody.create(MediaType.parse("multipart/form-data"), string)
    }
    var retrofit: Retrofit? = null

    private var authHeader: String = ""


    fun retrofit(authHeader: String): Apis {
        return createService(
            Apis::class.java,
            authHeader
        )
    }

    private val builder = Retrofit.Builder()
        .baseUrl(WebServicesConstants.BASE_URL)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())


    fun createService(
        serviceClass: Class<Apis>, authToken: String
    ): Apis {
        authHeader = authToken
        builder.client(fetchHeaders())

        retrofit = builder.build()


        return retrofit!!.create(serviceClass)
    }


    internal fun fetchHeaders(): OkHttpClient {
        //Define the interceptor, add authentication headers
        val interceptor = Interceptor { chain ->
            val builder = chain.request().newBuilder()

            val newRequest = builder.addHeader("Authorization",
                    "Bearer $authHeader"
            ).addHeader("Content-Type", "application/json")
              //  .addHeader("Accept", "application/json")
                .build()
            chain.proceed(newRequest)
        }

        // Add the interceptor to OkHttpClient
        val builder = OkHttpClient.Builder()
        builder.connectTimeout(20, TimeUnit.SECONDS)
        builder.readTimeout(20, TimeUnit.SECONDS)
        builder.writeTimeout(20, TimeUnit.SECONDS)


        val interceptor2 = HttpLoggingInterceptor()
        interceptor2.level = Level.BODY
        builder.addInterceptor(interceptor2).build()

       // builder.interceptors().add(interceptor2)
        builder.interceptors().add(interceptor)
        return builder.build()

    }


}

fun getRetrofit(accessToken: String): Apis {
    return RetrofitUtil.createService(
        Apis::class.java,
        accessToken
    )
}