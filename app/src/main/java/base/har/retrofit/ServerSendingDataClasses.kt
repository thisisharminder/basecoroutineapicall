package base.har.retrofit

import com.google.gson.annotations.SerializedName


data class RecoverAccount(var email: String)
data class AccountRecovery(var email:String,var otp: String)
data class NewPhoneNumber(var email: String,var phoneNumber: String,var countryCode:String)
data class VerifyNewPhoneNumber(var email: String,var phoneNumber: String,var otp: String,var countryCode: String)

data class UpdateUser(
    var dob: String,
    var email: String,
    var firstName: String,
    var gender: String,
    var id: Int,
    var lastName: String,
    var middleName: String,
    var userName: String
)

data class UserNameExsits(var userName: String)
data class LoginWithOtp(
    var authProvider: String,
    var otp: String,
    var userId: String,var formatPhoneNo:String,var countryCode:String
)

data class GetOTP(var phoneNumber: String,var formatPhoneNo:String,var countryCode:String)
data class FbAccountLogin(var profileId: String)
data class FbAccountSignup(
    var dob: String,
    var email: String,
    var firstName: String,
    var gender: String,
    var lastName: String,
    var middleName: String,
    var phoneNumber: String,
    var profileId: String,
    var provider: String,
    var userName: String
)
//For SignUp Organization
data class GetEmailOTP(val email: String)
data class NewEmailUser(var authProvider: String, var email: String, var otp: String)
data class UpdateOrgDetails(
    val address: String,
    val countryCode: String?,
    val email: String?,
    val formattedPhoneNumber: String,
    val imageUrl: String,
    val latitude: Int?,
    val longitude: Int?,
    val organisationId: String,
    val organisationName: String,
    val organisationType: String,
    val phone: String,
    val registrationCode: String?,
    val stateOfRegistration: String,
    val uploadedDocuments: List<String>?,
    val website: String
)
//For Follow Search
data class FollowSearch(
    @SerializedName("searchParam")
    val searchParam: String
)
data class FollowRequest(
    @SerializedName("userActionType")
    val userActionType: String,
    @SerializedName("userIds")
    val userIds: List<Int>
)