package base.har.ui.testcoroutine

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.MutableLiveData
import base.har.base.BaseDataModel
import base.har.base.BaseViewModel
import base.har.base.Coroutines
import base.har.retrofit.ErrorCustom
import base.har.retrofit2.Apifactory
import base.har.retrofit2.safeApiCall

class MainActivityViewModel(app: Application) : BaseViewModel(app) {


    val loginLiveData = MutableLiveData<BaseDataModel>()

    @SuppressLint("CheckResult")
    fun loginAPi() {

        call = Coroutines.main {
            val login = getParams()

            val result = safeApiCall(
                call = { Apifactory.tmdbApi.loginCoroutines(login).await() }
            )


            when(result)
            {
                is BaseDataModel ->{
                    loginLiveData.value = result
                }

                is ErrorCustom ->{
                    errorLiveData.value = result
                }

            }


        }
    }


    private fun getParams(): LoginServerCall {
        return LoginServerCall().apply {
            phone.apply {
                number = "8146415511"
                code = "+91"
            }
        }
    }



}

