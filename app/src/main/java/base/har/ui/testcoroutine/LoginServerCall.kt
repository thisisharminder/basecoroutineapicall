package base.har.ui.testcoroutine

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginServerCall {

    @SerializedName("phone")
    @Expose
    var phone= Phone()

}

class Phone {

    @SerializedName("code")
    @Expose
    var code: String? = null
    @SerializedName("number")
    @Expose
    var number: String? = null

}