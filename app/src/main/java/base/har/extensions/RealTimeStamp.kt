package base.har.extensions

import android.content.Context
import base.har.cache.PrefNames
import base.har.cache.Prefs
import java.util.*

/**
 * it attach time stamp in message according to the server time.
 */
fun generateTimeForChat(context: Context): Long {
    val elapsedTimeDifference =
        android.os.SystemClock.elapsedRealtime() - Prefs.with(context).getLong(PrefNames.BOOT_TIME_SYSTEM,0)// difference between current boot time and boot time saved when application was started
    if (elapsedTimeDifference < 0) { // if difference is negative, then  get current GMT time
        return GetUnixTime()
    }

    val timeToShow =
        elapsedTimeDifference + Prefs.with(context).getLong(PrefNames.SERVER_TIME,0) // generating current time by adding boot time difference into server time that is stored when app was started
    val calendar = Calendar.getInstance()
    //  TimeZone tz = TimeZone.getDefault();
    calendar.timeInMillis = timeToShow
    val year = calendar.get(Calendar.YEAR)
    return if (year == 1970) {
        GetUnixTime()
    } else timeToShow

}


/**
 * generating current GMT time
 *
 * @return current GMT time
 */
fun GetUnixTime(): Long {
    val calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"))
    return calendar.timeInMillis

}