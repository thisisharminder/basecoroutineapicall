package base.har.extensions

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.util.Log
import java.util.*

var errorMessage: String=""
fun getAddress(context: Context?, currentLocation: Location): String {
    val geocoder: Geocoder = Geocoder(context, Locale.getDefault())
    var addresses: List<Address> = emptyList()
    try {
        addresses = geocoder.getFromLocation(
                currentLocation.latitude,
                currentLocation.longitude,
                1)
    } catch (exception: Exception) {

        // Catch invalid latitude or longitude values.
       // errorMessage = context!!.getString(R.string.noLocation)
        Log.e("Location", "$errorMessage. Latitude = $currentLocation.latitude , " +
                "Longitude =  $currentLocation.longitude", exception)
    }


    if(addresses.isNullOrEmpty()) return ""
    val address = addresses[0]
    // Fetch the address lines using getAddressLine,
    // join them, and send them to the thread.
    val addressFragments = with(address) {
        (0..maxAddressLineIndex).map { getAddressLine(it) }
    }


    // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//    val city = addresses[0].getLocality()
//    val state = addresses[0].getAdminArea()
//    val country = addresses[0].getCountryName()
//    val postalCode = addresses[0].getPostalCode()
//    val knownName = addresses[0].getFeatureName() // Only if available else return NULL


    return addressFragments.joinToString(separator = "\n")
}


