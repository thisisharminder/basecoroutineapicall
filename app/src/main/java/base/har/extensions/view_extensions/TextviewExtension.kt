package base.har.extensions.view_extensions

import android.widget.TextView

val TextView.content: String
    get() {
        return text.toString().trim()
    }

