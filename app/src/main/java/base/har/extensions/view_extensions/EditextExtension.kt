package base.har.extensions.view_extensions

import android.widget.EditText

  val EditText.content: String
    get() {
        return text.toString().trim()
    }


val EditText.notEditable: EditText
    get() {
        isFocusable=false
        isCursorVisible=false
        isFocusableInTouchMode=false
        return this
    }