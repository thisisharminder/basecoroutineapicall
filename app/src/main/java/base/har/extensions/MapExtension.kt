package base.har.extensions

//package har.falcontag.extensions
//
//import android.app.Activity
//import android.content.res.Resources
//import android.os.Handler
//import android.os.SystemClock
//import android.view.animation.AccelerateDecelerateInterpolator
//import androidx.core.content.ContextCompat
//import com.google.android.gms.maps.CameraUpdate
//import com.google.android.gms.maps.CameraUpdateFactory
//import com.google.android.gms.maps.GoogleMap
//import com.google.android.gms.maps.model.*
//import sg.hopntrackdriver.R
//import sg.hopntrackdriver.ui.home.*
//import java.lang.Math.*
//
//var LINE_WIDTH = 12f
//fun GoogleMap.setMapStyling(homeActivity: Activity) {
//    try {
//        // Customise the styling of the base map using positionSelected JSON object defined
//        // in positionSelected raw resource file.
//        val success = this.setMapStyle(
//                MapStyleOptions.loadRawResourceStyle(
//                        homeActivity, R.raw.style_json
//                )
//        )
//
//        if (!success) {
//            Log.e(HomeActivity::class.java.canonicalName, "Style parsing failed.")
//        }
//    } catch (e: Resources.NotFoundException) {
//        Log.e(HomeActivity::class.java.canonicalName, "Can't find style. Error: ", e)
//    }
//
//}
//
//fun rad2deg(rad: Double): Double {
//    return rad * 180.0 / Math.PI
//}
//
//fun deg2rad(deg: Double): Double {
//    return (deg * Math.PI / 180.0);
//}
//
//
//fun GoogleMap.disableMarkerClicks(b: Boolean) {
//    this.setOnMarkerClickListener {
//        b
//    }
//}
//
//fun GoogleMap.getRoute(value: PojoRoute): ArrayList<LatLng> {
//
//    var routelist = ArrayList<LatLng>()
//    if (value.routes!!.isNotEmpty()) {
//        var decodelist: ArrayList<LatLng>
//        val routeA = value.routes!![0]
//        val time = value.routes!![0].legs!!.get(0).duration!!.text
//        // tvTime.setText(time)
//        val distance = value.routes!![0].legs!![0].distance!!.text
//        // tvDistance.setText("($distance)")
//        if (routeA.legs!!.isNotEmpty()) {
//            val steps = routeA.legs!![0].steps!!
//            var step: Step
//            var start_location: Start_location
//            var end_location: End_location
//            var polyline: String
//            for (i in steps.indices) {
//                step = steps.get(i)
//                start_location = step.start_location!!
//                routelist.add(LatLng(start_location.getLat(), start_location.getLng()))
//                polyline = step.polyline!!.points!!
//                decodelist = RouteDecode.decodePoly(polyline)
//                routelist.addAll(decodelist)
//                end_location = step.end_location!!
//                routelist.add(LatLng(end_location.getLat(), end_location.getLng()))
//
//            }
//        }
//    }
//    return routelist
//}
//
//fun GoogleMap.getSetPolyline(routelist: ArrayList<LatLng>, activity: Activity): Polyline? {
//    if (routelist.size > 0) {
//        val rectLine =
//            PolylineOptions().width(LINE_WIDTH).geodesic(true)
//                .color(ContextCompat.getColor(activity, sg.hopntrackdriver.R.color.mapPurple))
//
//        for (i in routelist.indices) {
//            rectLine.add(routelist[i])
//        }
//
//        val options1 = MarkerOptions()
//        options1.position(routelist[routelist.size - 1])
//
//        var polyline = this.addPolyline(rectLine)
//
//        return polyline
//
//    }
//    return null
//}
//
//fun GoogleMap.showFullPolyline(
//        routelist: ArrayList<LatLng>,
//        driverAssigned: Boolean
//) {
//    this.setPadding(200, 0, 0, 400)
//    var builder: LatLngBounds.Builder = LatLngBounds.Builder()
//    for (value in routelist)
//        builder.include(value)
//
//    if (!driverAssigned) {
//        var bounds: LatLngBounds = builder.build()
//        var cu: CameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 200)
//        this.animateCamera(cu)
//    }
//}
//
//private fun bearingBetweenLocations(latLng1: LatLng, latLng2: LatLng): Double {
//
//    val PI = 3.14159
//    val lat1 = latLng1.latitude * PI / 180
//    val long1 = latLng1.longitude * PI / 180
//    val lat2 = latLng2.latitude * PI / 180
//    val long2 = latLng2.longitude * PI / 180
//
//    val dLon = long2 - long1
//
//    val y = Math.sin(dLon) * Math.cos(lat2)
//    val x = Math.cos(lat1) * Math.sin(lat2) - (Math.sin(lat1)
//            * Math.cos(lat2) * Math.cos(dLon))
//
//    var brng = Math.atan2(y, x)
//
//    brng = Math.toDegrees(brng)
//    brng = (brng + 360) % 360
//
//    return brng
//}
//
//
//var isAnimationEnd=true
//@Synchronized
//fun animateMarkerToGB(
//    marker: Marker,
//    CurrentLatlng: LatLng,
//    latLngInterpolator: LatLngInterpolator, mMap: GoogleMap
//) {
//
//   if(!isAnimationEnd)
//       return
//
//    isAnimationEnd =false;
//
//    var toRotation = bearingBetweenLocations(marker.position, CurrentLatlng).toFloat()
//    val startPosition = marker.position
//    val start = SystemClock.uptimeMillis()
//    val interpolator = AccelerateDecelerateInterpolator()
//    val durationInMs = 2000f
//
//    val handler = Handler()
//
//    mMap.animateCamera(
//            CameraUpdateFactory.newCameraPosition(
//                    CameraPosition.Builder()
//                        .target(marker.position)
//                        .zoom(16f)
//                        .bearing(marker.rotation)
//                        .build()
//            ))
//
//    handler.post(object : Runnable {
//        var elapsed: Long = 0
//        var t: Float = 0.toFloat()
//        var v: Float = 0.toFloat()
//
//        override fun run() {
//            // Calculate progress using interpolator
//            elapsed = SystemClock.uptimeMillis() - start
//            t = elapsed / durationInMs
//            v = interpolator.getInterpolation(t)
//
//            val rot = t * toRotation + (1 - t) * marker.rotation
//
//            marker.position = latLngInterpolator.interpolate(v, startPosition, CurrentLatlng)
//
//            marker.rotation = if (-rot > 180) rot / 2 else rot
//
//
//            // Repeat till progress is complete.
//            if (t < 1) {
//                // Post again 16ms later.
//                handler.postDelayed(this, 16)
//            }
//            else {
//
//                isAnimationEnd = true
//            }
//        }
//    })
//}
//
//
//private interface LatLngInterpolatorNew {
//    fun interpolate(fraction: Float, positionSelected: LatLng, b: LatLng): LatLng
//
//    class LinearFixed : LatLngInterpolatorNew {
//        override fun interpolate(fraction: Float, positionSelected: LatLng, b: LatLng): LatLng {
//            val lat = (b.latitude - positionSelected.latitude) * fraction + positionSelected.latitude
//            var lngDelta = b.longitude - positionSelected.longitude
//            // Take the shortest path across the 180th meridian.
//            if (Math.abs(lngDelta) > 180) {
//                lngDelta -= Math.signum(lngDelta) * 360
//            }
//            val lng = lngDelta * fraction + positionSelected.longitude
//            return LatLng(lat, lng)
//        }
//    }
//}
//
////Method for finding bearing between two points
//private fun getBearing(begin: LatLng, end: LatLng): Float {
//    val lat = Math.abs(begin.latitude - end.latitude)
//    val lng = Math.abs(begin.longitude - end.longitude)
//
//    if (begin.latitude < end.latitude && begin.longitude < end.longitude)
//        return Math.toDegrees(Math.atan(lng / lat)).toFloat()
//    else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
//        return (90 - Math.toDegrees(Math.atan(lng / lat)) + 90).toFloat()
//    else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
//        return (Math.toDegrees(Math.atan(lng / lat)) + 180).toFloat()
//    else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
//        return (90 - Math.toDegrees(Math.atan(lng / lat)) + 270).toFloat()
//    return -1f
//}
//
//
//interface LatLngInterpolator {
//
//    fun interpolate(fraction: Float, positionSelected: LatLng, b: LatLng): LatLng
//
//    class Spherical : LatLngInterpolator {
//
//        /* From github.com/googlemaps/android-maps-utils */
//        override fun interpolate(fraction: Float, from: LatLng, to: LatLng): LatLng {
//            // http://en.wikipedia.org/wiki/Slerp
//            val fromLat = toRadians(from.latitude)
//            val fromLng = toRadians(from.longitude)
//            val toLat = toRadians(to.latitude)
//            val toLng = toRadians(to.longitude)
//            val cosFromLat = cos(fromLat)
//            val cosToLat = cos(toLat)
//
//            // Computes Spherical interpolation coefficients.
//            val angle = computeAngleBetween(fromLat, fromLng, toLat, toLng)
//            val sinAngle = sin(angle)
//            if (sinAngle < 1E-6) {
//                return from
//            }
//            val positionSelected = sin((1 - fraction) * angle) / sinAngle
//            val b = sin(fraction * angle) / sinAngle
//
//            // Converts from polar to vector and interpolate.
//            val x = positionSelected * cosFromLat * cos(fromLng) + b * cosToLat * cos(toLng)
//            val y = positionSelected * cosFromLat * sin(fromLng) + b * cosToLat * sin(toLng)
//            val z = positionSelected * sin(fromLat) + b * sin(toLat)
//
//            // Converts interpolated vector back to polar.
//            val lat = atan2(z, sqrt(x * x + y * y))
//            val lng = atan2(y, x)
//            return LatLng(toDegrees(lat), toDegrees(lng))
//        }
//
//        private fun computeAngleBetween(fromLat: Double, fromLng: Double, toLat: Double, toLng: Double): Double {
//            // Haversine's formula
//            val dLat = fromLat - toLat
//            val dLng = fromLng - toLng
//            return 2 * asin(sqrt(pow(sin(dLat / 2), 2.0) + cos(fromLat) * cos(toLat) * pow(sin(dLng / 2), 2.0)))
//        }
//    }
//}
//
//
//
//
//
//
