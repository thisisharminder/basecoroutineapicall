package base.har.extensions

import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.media.ThumbnailUtils
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.lang.Long.parseLong
import java.util.concurrent.TimeUnit

fun getRealPathFromUri(context: Context, contentUri: Uri): String {
    var cursor: Cursor? = null
    try {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        cursor = context.contentResolver.query(contentUri, proj, null, null, null)
        val column_index = cursor!!
            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        return cursor.getString(column_index)
    } finally {
        if (cursor != null) {
            cursor.close()
        }
    }
}
fun getMultipartBody(mPaths: java.util.ArrayList<String>?): MultipartBody.Part {

    val file = File("${mPaths?.first()}")

    val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)

    // MultipartBody.Part is used to send also the actual file name
    var body = MultipartBody.Part.createFormData(
        "file",
        file.name, requestFile
    )

    return body
}


fun getVideoPath(context: Context, uri: Uri): String? {
    var cursor:Cursor? = null
    try{
    val projection = arrayOf(MediaStore.Images.Media.DATA)
    cursor = context.contentResolver.query(uri, projection, null, null, null)
        val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Video.Media.DATA)
        cursor.moveToFirst()
        return cursor.getString(column_index)

    } finally {
        if(cursor!=null)
        {
            cursor.close()
        }
    }
}

fun getVideoDuration(context: Context, uri: Uri): String {
    val mediaMetadataRetriever = MediaMetadataRetriever()
    mediaMetadataRetriever.setDataSource(context,uri)
    var time = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
    Log.e("anshul","Time : $time")
    var timeInMillisec = parseLong(time)
    mediaMetadataRetriever.release()
    val minutes = String.format("%02d", TimeUnit.MILLISECONDS.toMinutes(timeInMillisec))
    val tempsec:Int = (minutes.toInt() * 60)
    val seconds = String.format("%02d", TimeUnit.MILLISECONDS.toSeconds(timeInMillisec)-tempsec)
    return "${minutes}:${seconds}"
}

//Sends thumbnail from video as multipart body without saving it locally.
fun getMultipartThumbnailFromVideo(selectedVideoPath:String):MultipartBody.Part
{
    val bitmap = ThumbnailUtils.createVideoThumbnail(selectedVideoPath, MediaStore.Video.Thumbnails.MICRO_KIND)
    val baos = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
    val imageBytes = baos.toByteArray()
    val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"),baos.toByteArray())
    // val encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT)
    var body = MultipartBody.Part.createFormData("file","IMG_" + System.currentTimeMillis() + ".jpg",requestFile)
    return body
}

